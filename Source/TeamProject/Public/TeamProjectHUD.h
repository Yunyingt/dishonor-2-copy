// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "Pickup.h"
#include "TeamProjectHUD.generated.h"

UCLASS()
class ATeamProjectHUD : public AHUD
{
	GENERATED_UCLASS_BODY()

public:

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY()
	UFont* HUDFont;

	void ShowStamina();

	void ShowInventory();

	void ShowPowerTimer();

	void AddInventory(APickup* p);
	void UseInventory(FString s);
	void ChooseInventory(int i);
	void ShowInventoryChoice();
	int CurrentChoice;

	UPROPERTY(EditAnywhere, Category = InventoryHUD)
	FVector2D InventoryDisplayPos;

	UPROPERTY(EditAnywhere, Category = InventoryHUD)
	float Offset;

	UPROPERTY(EditAnywhere, Category = InventoryHUD)
	float LeftOffset;

	UPROPERTY(EditAnywhere, Category = InventoryHUD)
	float UpOffset;

	UPROPERTY(EditAnywhere, Category = HUD)
	float Timer;

private:
	/** Crosshair asset pointer */
	UPROPERTY()
	class UTexture2D* CrosshairTex;

	UPROPERTY()
	class UMaterialInstanceDynamic* StaminaBarMI;

	UPROPERTY()
	class UMaterialInstanceDynamic* PowerTimerMI;

	UPROPERTY()
	TArray<APickup*> Inventories;

	UMaterial *InventoryBgrd;
	UMaterial* KeyInv;
	UMaterial* InvisibleInv;
	UMaterial* GhostInv;
	UMaterial* BottleInv;

};

