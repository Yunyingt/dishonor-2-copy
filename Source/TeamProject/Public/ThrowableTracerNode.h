

#pragma once

#include "GameFramework/Actor.h"
#include "ThrowableTracerNode.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AThrowableTracerNode : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Graphics)
	TSubobjectPtr<UStaticMeshComponent> ObjectMesh;
	
};
