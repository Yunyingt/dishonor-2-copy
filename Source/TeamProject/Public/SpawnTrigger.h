#pragma once

#include "GameFramework/Actor.h"
#include "GuardCharacter.h"
#include "SpawnTrigger.generated.h"

/**
*
*/
UCLASS()
class TEAMPROJECT_API ASpawnTrigger : public AActor
{
	GENERATED_UCLASS_BODY()

		TSubobjectPtr<UBoxComponent> Box;

	//UPROPERTY(EditAnywhere, Category = Sound)
	//	USoundCue* MySound;

	//UPROPERTY(Transient)
	//	UAudioComponent* MySoundAC;
	APlayerController* PC;

	UPROPERTY(EditAnywhere, Category = Spawn)
		TSubclassOf<class AGuardCharacter> SpawnClass;
	UPROPERTY(EditAnywhere, Category = Spawn)
		ATargetPoint* SpawnPoint;
	UPROPERTY(EditAnywhere, Category = Spawn)
		ATargetPoint* PlayerSpawnPoint;

	UPROPERTY(EditAnywhere, Category = Sound)
		USoundCue* MySound;
	UPROPERTY(Transient)
		UAudioComponent* MySoundAC;
	/*UPROPERTY(EditAnywhere, Category = Spawn)
	FVector SpawnLocation;
	UPROPERTY(EditAnywhere, Category = Spawn)
	FRotator SpawnRotation;*/
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* AttackAnim;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Spawn)
		TSubobjectPtr<USkeletalMeshComponent> SpawnMesh;

	bool spawnedOnce;
	AGuardCharacter* SpawnedGuard;


	UFUNCTION()
		void Debug(FString Msg);
	UFUNCTION()
		void TriggerEnter(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	//UFUNCTION()
	//	void TriggerExit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
		AGuardCharacter* SpawnGuard();
	UFUNCTION()
		void FadeIn();
	UFUNCTION()
		void FadeOut();
	UFUNCTION()
		UAudioComponent* PlayMySound(USoundCue* sound);

};