

#pragma once

#include "GameFramework/Actor.h"
#include "Throwable.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AThrowable : public AActor
{
	GENERATED_UCLASS_BODY()

	// Gameplay
	UPROPERTY(VisibleDefaultsOnly, Category = Gameplay)
	TSubobjectPtr<class USphereComponent> CollisionComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	TSubobjectPtr<class UProjectileMovementComponent> ProjectileMovement;
	UFUNCTION()
	void OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	bool isTracer;
	//float ZVelocity;
	//float velocitySquare;
	//bool bSetVelocity;

	// Graphics
	// Create mesh in blueprint

	// Sound
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* GetObjectSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* CrashObjectSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	float SoundRadius;

	// Method
	void Tick(float DeltaSeconds) override;
	
};
