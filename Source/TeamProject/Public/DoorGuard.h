// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseGuard.h"
#include "DoorGuard.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API ADoorGuard : public ABaseGuard
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = DoorGuard)
	FVector FirstLocation;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = DoorGuard)
	FVector SecondLocation;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = DoorGuard)
	FVector Destination;

	virtual void Move() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
};
