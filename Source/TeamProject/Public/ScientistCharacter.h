

#pragma once

#include "EnemyCharacter.h"
#include "ScientistCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AScientistCharacter : public AEnemyCharacter
{
	GENERATED_UCLASS_BODY()

	void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Guard)
		bool SightlineDetection();
	
	UFUNCTION(BlueprintCallable, Category = Guard)
		void HearBottle(FVector Location);

	//stores Self Destruct button actor
	UPROPERTY(EditAnywhere, Category = Effects)
		AActor* SDButton;

	UPROPERTY(EditAnywhere, Category = Effects)
		UParticleSystem* ExplosionActor;

	UPROPERTY(EditAnywhere, Category = Effects)
		AActor* ExplosionLocation;


	UPROPERTY(EditAnywhere, Category = Effects)
		USoundCue* ExplosionCue;

	UPROPERTY(EditAnywhere, Category = Effects)
		float ExplosionRadius;

	void Detonate();

};
