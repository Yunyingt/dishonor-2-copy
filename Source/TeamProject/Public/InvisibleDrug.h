

#pragma once

#include "Drugs.h"
#include "InvisibleDrug.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AInvisibleDrug : public ADrugs
{
	GENERATED_UCLASS_BODY()


	void OnUse();

	// Method
	void Tick(float DeltaSeconds) override;
	
};
