

#pragma once

#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API APickup : public AActor
{
	GENERATED_UCLASS_BODY()

	// Graphics
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Graphics)
	TSubobjectPtr<UStaticMeshComponent> ObjectMesh;

	// Sound
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* GetObjectSound;
	
	// Method
	void Tick(float DeltaSeconds) override;
	UFUNCTION()
	void OnStartFlash();
	UFUNCTION()
	void OnStopFlash();

	float flashingTime;
	float timeFlashed;
	enum State
	{
		GLOWING, ANTIGLOWING
	};
	State mState;
};
