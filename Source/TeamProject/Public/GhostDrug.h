

#pragma once

#include "Drugs.h"
#include "GhostDrug.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AGhostDrug : public ADrugs
{
	GENERATED_UCLASS_BODY()

	virtual void OnUse() override;
	
};
