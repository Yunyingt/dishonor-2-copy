

#pragma once

#include "AIEnemyController.h"
#include "AIGuardController.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AAIGuardController : public AAIEnemyController
{
	GENERATED_UCLASS_BODY()

	void BeginPlay() override;
	void Tick(float deltaTime) override;
	void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;
	
	//AGuardCharacter* MyCharacter;
	//move state machine forward
	void StateChange();




	void ChangeStatePatrol();






	UFUNCTION(BlueprintCallable, Category = Patrol)
		void NextPatrolPoint();

	UFUNCTION(BlueprintCallable, Category = Attack)
		void KillPlayer();



	//called by GuardCharacter from Throwable right as the Throwable is destroyed
	//UFUNCTION(BlueprintCallable, Category = Patrol)
	//	void HearBottle(FVector Location);

	
};
