

#pragma once

#include "AIController.h"
#include "AIEnemyController.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AAIEnemyController : public AAIController
{
	GENERATED_UCLASS_BODY()

	//points to patrol through
	UPROPERTY(EditAnywhere, Category = Patrol)
	TArray<AActor*> TargetPoints;
	//increment to cycle through patrol points
	int patrolIdx;

	UPROPERTY(EditAnywhere, Category = search)
	float SearchTime;

	//last known location of player
	FVector lastPlayerLocation;

	//controls AI behavior in tracking player
	enum State { Start, Patrol, Investigate, Chase, Attack, Dead, End };
	State CurrentState;

	void HearBottle(FVector Location);

	APawn* PlayerPawn;
	APawn* MyPawn;

	

};
