

#pragma once

#include "Pickup.h"
#include "Drugs.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API ADrugs : public APickup
{
	GENERATED_UCLASS_BODY()

	float Time;

	// Graphics
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Graphics)
	TSubobjectPtr<UStaticMeshComponent> DrugMesh;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = ShipEquipment)
	TSubobjectPtr<UBoxComponent> TriggerVolume;

	// Method
	virtual void Tick(float DeltaSeconds) override;

	virtual void OnUse();
	
};
