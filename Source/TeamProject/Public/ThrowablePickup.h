

#pragma once

#include "Pickup.h"
#include "ThrowablePickup.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AThrowablePickup : public APickup
{
	GENERATED_UCLASS_BODY()
};
