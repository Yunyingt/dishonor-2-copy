// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "BaseGuard.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
namespace EGuardState
{
	enum State
	{
		WAITING UMETA(DisplayName = "Waiting"),
		DETECTED UMETA(DisplayName = "Detected"),
		CHASE UMETA(DisplayName = "Chase"),
		ATTACK UMETA(DisplayName = "Attack"),
		DEAD UMETA(DisplayName = "Dead")
	};
}

UCLASS()
class TEAMPROJECT_API ABaseGuard : public ACharacter
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = BaseGuard)
	TSubobjectPtr<UStaticMeshComponent> GuardMesh;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = BaseGuard)
	TSubobjectPtr<UBoxComponent> DetectVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseGuard)
	float MaxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseGuard)
	float CurrentHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseGuard)
	float MovementSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = BaseGuard)
	float SpeedChange;

	UFUNCTION(BlueprintCallable, Category = BaseGuard)
	virtual void ApplySpeedChange(float c);

	UFUNCTION(BlueprintCallable, Category = BaseGuard)
	virtual void Move();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = ShipEquipment)
	TEnumAsByte<EGuardState::State> GuardState;

	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
};
