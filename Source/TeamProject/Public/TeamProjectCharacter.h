// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "Runtime/Core/Public/Core.h"
#include "Throwable.h"
#include "Pickup.h"
#include "ThrowableTracerNode.h"
#include "TeamProjectCharacter.generated.h"

UCLASS(config=Game)
class ATeamProjectCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	TSubobjectPtr<class USkeletalMeshComponent> Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	TSubobjectPtr<class UCameraComponent> FirstPersonCameraComponent;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<class AThrowable> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	TArray<APickup*> Inventory;

	// Gamepaly
	bool bHaveKey;
	bool bInteractObject;
	bool bHaveThrowable;

	// Throwing trajectory
	AThrowable* TracerClass;
	FVector oldLocation;
	FRotator oldRotation;
	float throwRange;

	void StartDrawingTracerNodes();
	void StopDrawingTracerNodes();
	UFUNCTION()
	void DrawTracerNodes();
	UFUNCTION()
	void ClearTracerNodes();
	void CreateTracerNode();
	AThrowable* ThrowTracer();
	TArray<AThrowableTracerNode*> TracerNodes;
	AThrowableTracerNode* TracerNode;
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<AThrowableTracerNode> TracerNodeBP;

	int curTracerNodeCount;
	int maxTracerNodeCount;
	bool drawingTracer;

	//Stamina Manipulation
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Stamina)
	float Stamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stamina)
	float Stamina_Max;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stamina)
	float S_Reduction_Rate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stamina)
	float S_Regen_Rate;

	//true when crouching, seen by Guard
	bool Sneaking;
	bool IsSneaking();

	APlayerController* PC;
	//prekill is called to stop player movement, and lerp to look at guard as he kills you
	void PreKill();
	void Kill();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		bool dead;

	//seen by guard ai
	bool isInvisible();

	void AddPickup(APickup* p);
	void UseInventory(FString s);
	void ChooseInventory();
	void UseDrug();
	int CurrentChoice;
	int InventorySize;
	float ZLocation;

	UFUNCTION(BlueprintCallable, Category = Ghost)
	void SetGhost();

	UFUNCTION(BlueprintCallable, Category = Ghost)
	void StopGhost();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Ghost)
	void AllowGhostThroughWall();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Ghost)
	void EndAllowGhostThroughWall();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Ghost)
	bool GhostState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Guard)
		AActor* GuardScareLocation;

	/** Become invisible due to drug! timer. toggle when pressed again?**/
	void OnStartInvisibility();

protected:

	/** Handler for a touch input beginning. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Throw a throwable. */
	void OnFire(); // display trace
	void OnStopFire(); // throw the throwable
    
    /** Sneak **/
    void OnStartSneak();
    
    /** Stop Sneak **/
    void OnStopSneak();


	//TEST CHANGE
	void doesNothing();

	/**called by timer when invisibility expires**/
	void OnStopInvisibility();
	//bool to say if can be seen, can still be heard, when invisibile
	bool Invisible;
	 
	// Interact with object
	void OnStartInteractObject();
	void OnEndInteractObject();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	//turn around for scare
	void TurnAround();



	void PlayDeath();

	void Tick(float DeltaSeconds) override;



	float DeathTiming;

	void DeathFlash1();

	void DeathFlash2();

	UFUNCTION(BlueprintNativeEvent)
	void RestartGame();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface
};

