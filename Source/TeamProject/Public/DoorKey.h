

#pragma once

#include "Pickup.h"
#include "GameFramework/Actor.h"
#include "DoorKey.generated.h"


/**
 * 
 */
UCLASS()
class TEAMPROJECT_API ADoorKey : public APickup
{
	GENERATED_UCLASS_BODY()
};
