

#pragma once

#include "EnemyCharacter.h"
#include "GuardCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AGuardCharacter : public AEnemyCharacter
{
	GENERATED_UCLASS_BODY()


	void BeginPlay() override;





	//



	/* There volumn for character detection*/
	//UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Guard)
	//TSubobjectPtr<UConeComponent> DetectVolume;

	bool isChasing;
	
public:
	

	UFUNCTION(BlueprintCallable, Category = Guard)
	void OnDetectVolumnBeginOverlap(AActor* c);



	UFUNCTION(BlueprintCallable, Category = Guard)
	void OnDetectVolumnEndOverlap(AActor* c);

	UFUNCTION(BlueprintCallable, Category = Guard)
	void PlayChase(/*const FVector& v*/);

	UFUNCTION(BlueprintCallable, Category = Guard)
	void StopChase();

	//switches from walk to run and vice versa
	//void ToggleMoveSpeedWalk();
	//void ToggleMoveSpeedRun();

	UFUNCTION(BlueprintCallable, Category = Guard)
	bool SightlineDetection();

	UFUNCTION(BlueprintCallable, Category = Guard)
	bool ConeSightlineDetection();

	UFUNCTION(BlueprintCallable, Category = Guard)
	void HearBottle(FVector Location);


	//to play chase sequence music
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* ChaseLoopSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* ChaseFinishSound;
	UPROPERTY(Transient)
	UAudioComponent* ChaseAC;

	//Just for debugging purpose...
	void PlayHitEffect();

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* MuzzleFX;


	UParticleSystemComponent* PlayHitEffect(UParticleSystem* Particle, FVector end);
	
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* HitEF;

	UPROPERTY(Transient)
	UParticleSystemComponent* HitEffect;

protected:
	UAudioComponent* PlayChaseSound(USoundCue*);
	void StopChaseSound(USoundCue*);
};
