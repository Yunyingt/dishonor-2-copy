

#pragma once

#include "GameFramework/Actor.h"
#include "DoorWithKey.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API ADoorWithKey : public AActor
{
	GENERATED_UCLASS_BODY()

	// Graphics
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Graphics)
	TSubobjectPtr<UStaticMeshComponent> DoorMesh;

	// Audio
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* OpenDoorSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* LockedDoorSound;

	// Gameplay
	enum state
	{
		Closed, Opened
	};
	state DoorState;

	// Method
	void Tick(float DeltaSeconds) override;
	UFUNCTION()
	void RotateDoor();
};
