

#pragma once

#include "GameFramework/Actor.h"
#include "AudioTrigger.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AAudioTrigger : public AActor
{
	GENERATED_UCLASS_BODY()

	TSubobjectPtr<UBoxComponent> Box;

	UPROPERTY(EditAnywhere, Category = Sound)
	USoundCue* MySound;

	UPROPERTY(Transient)
		UAudioComponent* MySoundAC;

	UFUNCTION()
		void Debug(FString Msg);
	UFUNCTION()
		void TriggerEnter(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
		void TriggerExit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
		UAudioComponent* PlayMySound(USoundCue* sound);
	
};
