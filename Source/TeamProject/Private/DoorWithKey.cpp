

#include "TeamProject.h"
#include "DoorWithKey.h"
#include "TeamProjectCharacter.h"

ADoorWithKey::ADoorWithKey(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	DoorMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("DoorMesh"));
	RootComponent = DoorMesh;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	DoorState = Closed;
}

void ADoorWithKey::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (IsOverlappingActor(playerActor))
	{
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (DoorState == Closed && player->bHaveKey && player->bInteractObject)
		{
			GetWorldTimerManager().SetTimer(this, &ADoorWithKey::RotateDoor, GetWorld()->DeltaTimeSeconds, true);
			player->UseInventory("Key");
		}
	}
}

void ADoorWithKey::RotateDoor()
{
	FRotator DoorOpenDegree;
	DoorOpenDegree = FMath::RInterpTo(GetActorRotation(), FRotator(0.0, -180.0, 0.0), GetWorld()->DeltaTimeSeconds, 1.5);
	SetActorRotation(DoorOpenDegree);
	if (DoorOpenDegree.IsNearlyZero())
	{
		DoorState = Opened;
		GetWorldTimerManager().ClearTimer(this, &ADoorWithKey::RotateDoor);
	}
}

