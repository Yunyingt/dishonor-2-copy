// Fill out your copyright notice in the Description page of Project Settings.

#include "TeamProject.h"
#include "BaseGuard.h"


ABaseGuard::ABaseGuard(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	GuardMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("GuardMesh"));
	SetRootComponent(GuardMesh);

	DetectVolume = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("DetectVolume"));
	DetectVolume->AttachTo(RootComponent);

	PrimaryActorTick.bCanEverTick = true;

	MaxHP = 100;

	CurrentHP = 100;

	MovementSpeed = 10.0f;

	SpeedChange = 0.0f;

}

void ABaseGuard::BeginPlay(){
	Super::BeginPlay();
	GuardState = EGuardState::WAITING;
}

void ABaseGuard::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
}

void ABaseGuard::ApplySpeedChange(float c){
	SpeedChange = c;
}

void ABaseGuard::Move(){

}
