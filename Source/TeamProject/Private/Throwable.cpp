

#include "TeamProject.h"
#include "Throwable.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "TextureResource.h"
#include "GuardCharacter.h"


AThrowable::AThrowable(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Use a sphere as a simple collision representation
	CollisionComp = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(7.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Throwable");			// Collision profiles are defined in DefaultEngine.ini
	CollisionComp->OnComponentHit.AddDynamic(this, &AThrowable::OnHit);		// set up a notification for when this component overlaps something
	RootComponent = CollisionComp;

	// Use projectile movement
	ProjectileMovement = PCIP.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ThrowableComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 800.0f; // comment out later
	ProjectileMovement->MaxSpeed = 800.0f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = 1;

	isTracer = false;

	//ZVelocity = 50.0f;
	//bSetVelocity = true;

	//PrimaryActorTick.bCanEverTick = true;
	//PrimaryActorTick.bStartWithTickEnabled = true;
}

void AThrowable::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	/*
	if (bSetVelocity)
	{
		float XVelocity = (velocitySquare - ZVelocity*ZVelocity > 0.0f ? sqrt(velocitySquare - ZVelocity*ZVelocity) : 400.0f);
		ProjectileMovement->Velocity = FVector(XVelocity, 0.0f, ZVelocity);
		bSetVelocity = false;
	}
	*/
}

void AThrowable::OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	//if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	if (OtherActor != this)
	{
		// play sound
		if (isTracer)
		{
			
		}
		else if (CrashObjectSound != NULL)
		{
			UGameplayStatics::PlaySoundAtLocation(this, CrashObjectSound, GetActorLocation());

			//now find all enemies close enough to hear by iterating through all enemies
			TSubclassOf<AEnemyCharacter> EnemyClass;
			TArray<AActor*> AllEnemies = TArray<AActor*>();
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyCharacter::StaticClass(), AllEnemies);//populates Tarray of AllEnemies
			for (int i = 0; i < AllEnemies.Num();  i++)
			{
				float distanceToEnemy = GetDistanceTo(AllEnemies[i]);
				if (distanceToEnemy < SoundRadius)
				{
					AEnemyCharacter* GuardEnemy = Cast<AEnemyCharacter>(AllEnemies[i]);
					GuardEnemy->HearBottle(this->GetActorLocation());
					//send message to enemy saying they heard you
				}
			}
			Destroy();
		}
		// TODO: enemy detection
	}
}
