

#include "TeamProject.h"
#include "AIScientistController.h"
#include "ScientistCharacter.h"
#include "Engine.h"
#include "TeamProjectCharacter.h"

AAIScientistController::AAIScientistController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	CurrentState = Start;

	SearchTime = 5.0f;
	patrolIdx = 0;
}

void AAIScientistController::BeginPlay()
{
	Super::BeginPlay();
	UWorld* World = GetWorld();
	if (World)
	{
		MyPawn = GetPawn();
		if (MyPawn)
		{
			MyCharacter = Cast<AScientistCharacter>(MyPawn);
			if (MyCharacter)
			{
				TargetPoints = MyCharacter->GetPatrolPoints();
				SDButton = MyCharacter->SDButton;
			}
			//get player pawn
			PlayerPawn = UGameplayStatics::GetPlayerPawn(World, 0);
			if (PlayerPawn)
			{
				//AGuardCharacter* MyPawn = Cast<AGuardCharacter>(GetPawn()); 
				//MyPawn->SetActorLocation(PlayerPawn->GetActorLocation());
				//MoveToActor(PlayerPawn);
			}
		}

	}
}

void AAIScientistController::Tick(float deltaTime)
{
	if (!MyPawn)
	{
		return;
	}
	MyCharacter = Cast<AScientistCharacter>(MyPawn);//PROBLEM
		
	ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);

	//just spawned, start patrol
	if (CurrentState == Start)
	{
		CurrentState = Patrol;
	}

	//default state, walks to each set patrol point, stops for some time, then goes to next one in a cycle
	if (CurrentState == Patrol)
	{
		if (MyPawn)
		{
			MyCharacter->ToggleMoveSpeedWalk();

			MoveToActor(TargetPoints[patrolIdx]);
			if (PlayerPawn)
			{
				//if the player is in my line of sight
				//if (MyPawn->GetDistanceTo(PlayerPawn) < SightRadius)
				//{
				//	CurrentState = Investigate;
				//}
			}
		}


	}

	//investigate, move towards seen location of player, pause, look around, return to patrol
	if (CurrentState == Investigate)
	{

		if (PlayerPawn)
		{
			if (MyCharacter->SightlineDetection() && !PlayerCharacter->isInvisible())//if i see character, pursue
			{
				//this will change state so scientist will run to self destruct button
				StateChange();
			}
			//set timer, if dont see player, go back to patrol
			MoveToLocation(lastPlayerLocation);
			//MoveToActor(PlayerPawn);
		}
	}


	//see player while investigating, then run to self destruct!
	if (CurrentState == Chase)
	{
		if (SDButton)
		{
			MoveToLocation(SDButton->GetActorLocation());
		}
	}

	//this is called after sprinting to the self destruct button, plays attack animation to press, and then waits for death
	if (CurrentState == Attack)
	{
		CurrentState = End;
		ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
		if (PlayerCharacter)
		{
			//PlayerCharacter->Kill();
			if (!GetWorldTimerManager().IsTimerActive(this, &AAIScientistController::PressSelfDestructButton))
			{
				GetWorldTimerManager().SetTimer(this, &AAIScientistController::PressSelfDestructButton, MyCharacter->PlayAttack(), false);
			}

		}
	}

}

void AAIScientistController::StateChange()
{
	//from investigating, Guard will either SEE player and CHASE, or timer run out, and  back to PATROL
	GetWorldTimerManager().ClearAllTimersForObject(this);
	AScientistCharacter* MyCharacter = Cast<AScientistCharacter>(MyPawn);

	if (CurrentState == Investigate)
	{
		MyCharacter->ToggleMoveSpeedRun();
		CurrentState = Chase;
	}
	else if (CurrentState == Chase)
	{
		CurrentState = Attack;
	}
}

void AAIScientistController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	AScientistCharacter* MyCharacter = Cast<AScientistCharacter>(MyPawn);
	if (CurrentState == Investigate)
	{
		if (!GetWorldTimerManager().IsTimerActive(this, &AAIScientistController::ChangeStatePatrol))
		{
			GetWorldTimerManager().SetTimer(this, &AAIScientistController::ChangeStatePatrol, SearchTime, false);
		}
		//if im close to player, chase player
		if (PlayerPawn)
		{
			float distToPlayer = MyPawn->GetDistanceTo(PlayerPawn);
			ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
			if (MyCharacter->SightlineDetection() && !PlayerCharacter->isInvisible())
			{
				StateChange();
				//we got you player! goes into chase state
			}
			else if (distToPlayer < MyCharacter->SoundRadius && !PlayerCharacter->IsSneaking())
			{
				lastPlayerLocation = PlayerPawn->GetActorLocation();
			}
		}
	}
	if (CurrentState == Patrol)
	{

		float distance = MyPawn->GetDistanceTo(TargetPoints[patrolIdx]);
		//go around to each patrol point
		if (distance <  150.0f)		
		{
			if (!GetWorldTimerManager().IsTimerActive(this, &AAIScientistController::NextPatrolPoint))
			{
				GetWorldTimerManager().SetTimer(this, &AAIScientistController::NextPatrolPoint, SearchTime, false);
			}

		}
		//if im close to player, and i detect the player, investigate to be certain
		if (PlayerPawn)
		{
			float distToPlayer = MyPawn->GetDistanceTo(PlayerPawn);
			ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
			//if I hear player, investigate. if i dont see anything for some TIME, go back to patrolling
			if (distToPlayer <  MyCharacter->SoundRadius && !PlayerCharacter->IsSneaking())
			{
				lastPlayerLocation = PlayerPawn->GetActorLocation();
				GetWorldTimerManager().SetTimer(this, &AAIScientistController::ChangeStatePatrol, SearchTime, false);
				CurrentState = Investigate;

				//we got you player!
			}
		}
	}

	//if im certain i saw the character, run to self destruct button
	if (CurrentState == Chase)
	{

		//if im close to player, chase player
		if (SDButton)
		{
			float distToButton = MyPawn->GetDistanceTo(SDButton);
			
			//if im right at the button, hit it! changes state to Attack
			if (distToButton < MyCharacter->AttackRange)
			{
				StateChange();
			}

		}
	}

	//patrolIdx++;
}

void AAIScientistController::ChangeStatePatrol()
{
	CurrentState = Patrol;
}


void AAIScientistController::PressSelfDestructButton()
{
	AScientistCharacter* MyCharacter = Cast<AScientistCharacter>(MyPawn);
	MyCharacter->Detonate();
}


void AAIScientistController::NextPatrolPoint()
{
	patrolIdx++;
	if (patrolIdx >= TargetPoints.Num())
	{
		patrolIdx = patrolIdx - TargetPoints.Num();
	}
}