

#include "TeamProject.h"
#include "InvisibleDrug.h"
#include "TeamProjectCharacter.h"

AInvisibleDrug::AInvisibleDrug(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	//DrugMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("KeyMesh"));
	//RootComponent = DrugMesh;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AInvisibleDrug::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (IsOverlappingActor(playerActor))
	{
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (player->bInteractObject)
		{
			// Send event to player
			player->AddPickup(this);
		//	player->InvDrugCount++;
			// Destroy the key
			Destroy();
		}
	}
}

void AInvisibleDrug::OnUse(){
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Invisible On Use")));
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
	player->OnStartInvisibility();
}

