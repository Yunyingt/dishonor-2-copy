

#include "TeamProject.h"
#include "TeamProjectCharacter.h"
#include "Pickup.h"
#include "ThrowablePickup.h"
#include "SoundDefinitions.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"


APickup::APickup(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Graphics
	ObjectMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("ObjectMesh"));
	RootComponent = ObjectMesh;

	flashingTime = 2.0;
	timeFlashed = 0.0;
	mState = GLOWING;

	// Sound
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinder<USoundCue> soundCue;
		FConstructorStatics() :soundCue(TEXT("SoundCue'/Game/Audio/getObject_Cue'")){}
	};
	static FConstructorStatics ConstructorStatics;
	GetObjectSound = ConstructorStatics.soundCue.Object;
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void APickup::Tick(float DeltaSeconds)
{
	// Flashing
	if (mState == GLOWING)
		OnStartFlash();
	else OnStopFlash();

	// Interacting with player
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (IsOverlappingActor(playerActor))
	{
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (player->bInteractObject)
		{
			// Send event to player
			player->AddPickup(this);
			// play the sound
			UGameplayStatics::PlaySound(this, GetObjectSound, RootComponent);
			// Destroy the object
			Destroy();
			return;
		}
	}
}

void APickup::OnStartFlash()
{
	float brightness = FMath::Lerp(0.0, 5.0, timeFlashed / flashingTime);
	timeFlashed += GetWorld()->DeltaTimeSeconds;

	UMaterialInstanceDynamic* MaterialID;
	for (int i = 0; i < ObjectMesh->GetNumMaterials(); i++)
	{
		MaterialID = ObjectMesh->CreateAndSetMaterialInstanceDynamic(i);
		MaterialID->SetScalarParameterValue(FName(TEXT("Emissiveness")), brightness);
	}

	if (timeFlashed >= flashingTime)
		mState = ANTIGLOWING;
}

void APickup::OnStopFlash()
{
	float brightness = FMath::Lerp(0.0, 5.0, timeFlashed / flashingTime);
	timeFlashed -= GetWorld()->DeltaTimeSeconds;

	UMaterialInstanceDynamic* MaterialID;
	for (int i = 0; i < ObjectMesh->GetNumMaterials(); i++)
	{
		MaterialID = ObjectMesh->CreateAndSetMaterialInstanceDynamic(i);
		MaterialID->SetScalarParameterValue(FName(TEXT("Emissiveness")), brightness);
	}

	if (timeFlashed <= 0.0)
		mState = GLOWING;
}

