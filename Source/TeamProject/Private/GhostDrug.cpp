

#include "TeamProject.h"
#include "TeamProjectCharacter.h"
#include "GhostDrug.h"


AGhostDrug::AGhostDrug(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	
}

void AGhostDrug::OnUse(){
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Ghost On Use")));
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
	player->SetGhost();
}


