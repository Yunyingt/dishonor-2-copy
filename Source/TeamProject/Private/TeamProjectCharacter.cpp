// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "TeamProject.h"
#include "TeamProjectCharacter.h"
#include "TeamProjectProjectile.h"
#include "Throwable.h"
#include "DoorKey.h"
#include "ThrowableTracerNode.h"
#include "Animation/AnimInstance.h"
#include "TeamProjectHUD.h"
#include "ThrowablePickup.h"
#include "Engine.h"
#include "InvisibleDrug.h"
#include "GhostDrug.h"
#include <iostream>


//////////////////////////////////////////////////////////////////////////
// ATeamProjectCharacter

ATeamProjectCharacter::ATeamProjectCharacter(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Set size for collision capsule
	CapsuleComponent->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = PCIP.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->AttachParent = CapsuleComponent;
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 64.f); // Position the camera

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 30.0f, 10.0f);

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = PCIP.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	Mesh1P->AttachParent = FirstPersonCameraComponent;
	Mesh1P->RelativeLocation = FVector(0.f, 0.f, -150.f);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;

	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	


	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P are set in the
	// derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	bHaveKey = false;
	bInteractObject = false;
	bHaveThrowable = false; // to be changed to false
	TracerNodes.Reset(15);
	curTracerNodeCount = 0;
	maxTracerNodeCount = 14;
	throwRange = 500.0f;
	PrimaryActorTick.bCanEverTick = true;

	dead = false;
	DeathTiming = 0;

	Sneaking = false;

	Stamina = 100.0f;
	Stamina_Max = 100.0f;
	S_Reduction_Rate = 8.0f;
	S_Regen_Rate = 3.0f;

	CurrentChoice = 0;
	GhostState = false;
	
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATeamProjectCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// set up gameplay key bindings
	check(InputComponent);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	
	InputComponent->BindAction("Fire", IE_Pressed, this, &ATeamProjectCharacter::OnFire);
	InputComponent->BindAction("Fire", IE_Released, this, &ATeamProjectCharacter::OnStopFire);
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATeamProjectCharacter::TouchStarted);

	InputComponent->BindAxis("MoveForward", this, &ATeamProjectCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATeamProjectCharacter::MoveRight);
    
    InputComponent->BindAction("Sneak", IE_Pressed, this, &ATeamProjectCharacter::OnStartSneak);
    InputComponent->BindAction("Sneak", IE_Released, this, &ATeamProjectCharacter::OnStopSneak);

	InputComponent->BindAction("InteractObject", IE_Pressed, this, &ATeamProjectCharacter::OnStartInteractObject);
	InputComponent->BindAction("InteractObject", IE_Released, this, &ATeamProjectCharacter::OnEndInteractObject);

	InputComponent->BindAction("BecomeInvisible", IE_Pressed, this, &ATeamProjectCharacter::OnStartInvisibility);
	//InputComponent->BindAction("BecomeInvisible", IE_Released, this, &ATeamProjectCharacter::OnStopInvi);

	InputComponent->BindAction("UseObject", IE_Pressed, this, &ATeamProjectCharacter::UseDrug);
	InputComponent->BindAction("ChooseObject", IE_Pressed, this, &ATeamProjectCharacter::ChooseInventory);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &ATeamProjectCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &ATeamProjectCharacter::LookUpAtRate);

	Invisible = false;

	Inventory.Init(NULL, 6);
	CurrentChoice = -1;
}

void ATeamProjectCharacter::OnStartSneak()
{

	CharacterMovement->bWantsToCrouch = true;
	Sneaking = true;
	
}

void ATeamProjectCharacter::OnStopSneak()
{
    CharacterMovement->bWantsToCrouch = false;
	Sneaking = false;
}

void ATeamProjectCharacter::OnStartInvisibility()
{
	float stealthAlpha = 0.5f;
	UMaterialInstanceDynamic* SkinMaterialID;
	SkinMaterialID = Mesh1P->CreateAndSetMaterialInstanceDynamic(0);
	SkinMaterialID->SetScalarParameterValue(FName(TEXT("Opacity")), stealthAlpha);
	Invisible = true;
	GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::OnStopInvisibility,10, false);
	
}

void ATeamProjectCharacter::OnStopInvisibility()
{
	Invisible = false;
	float stealthAlpha = 1.0f;
	UMaterialInstanceDynamic* SkinMaterialID;
	SkinMaterialID = Mesh1P->CreateAndSetMaterialInstanceDynamic(0);
	SkinMaterialID->SetScalarParameterValue(FName(TEXT("Opacity")), stealthAlpha);
}

bool ATeamProjectCharacter::isInvisible()
{
	return Invisible;
}

bool ATeamProjectCharacter::IsSneaking()
{
	return Sneaking;
}

void ATeamProjectCharacter::OnStartInteractObject()
{
	bInteractObject = true;
}

void ATeamProjectCharacter::OnEndInteractObject()
{
	bInteractObject = false;
}

void ATeamProjectCharacter::StartDrawingTracerNodes()
{
	//already running
	if (GetWorldTimerManager().IsTimerActive(this, &ATeamProjectCharacter::DrawTracerNodes))
		return;

	//clear any remaining previous path nodes
	ClearTracerNodes();
	//throw the tracer
	TracerClass = ThrowTracer();
	//hide
	TracerClass->SetActorHiddenInGame(true);
	TracerClass->isTracer = true;

	curTracerNodeCount = 0;
	oldLocation = GetActorLocation();
	oldRotation = GetActorRotation();
	drawingTracer = true;

	GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::DrawTracerNodes, 0.07f, true); // Time between tracer node
	GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::ClearTracerNodes, 12.0f, false); // Time before cleaning up
}

void ATeamProjectCharacter::StopDrawingTracerNodes()
{
	if (TracerClass != NULL){
		TracerClass->Destroy();
	}
	GetWorldTimerManager().ClearTimer(this, &ATeamProjectCharacter::DrawTracerNodes);
	ClearTracerNodes();
}

void ATeamProjectCharacter::DrawTracerNodes()
{
	//end case
	if (curTracerNodeCount >= maxTracerNodeCount)
	{
		GetWorldTimerManager().ClearTimer(this, &ATeamProjectCharacter::DrawTracerNodes);
		return;
	}

	//lost tracer case
	if (TracerClass == NULL) 
	{
		GetWorldTimerManager().ClearTimer(this, &ATeamProjectCharacter::DrawTracerNodes);
		return;
	}

	CreateTracerNode();
}

void ATeamProjectCharacter::CreateTracerNode()
{
	//end case
	if (curTracerNodeCount >= maxTracerNodeCount)
		return;

	//lost the tracer
	if (TracerClass == NULL) 
		return;

	TracerNode = GetWorld()->SpawnActor<AThrowableTracerNode>(TracerNodeBP, TracerClass->GetActorLocation(), TracerClass->GetActorRotation());

	//code to set a material instance for your path node, as I use in video to make
	// the object glow different colors through code
	//pnode.staticmeshcomponent.setMaterial(0, pc.thirdpTargetingMat);

	TracerNodes.Insert(TracerNode, curTracerNodeCount);
	curTracerNodeCount++;
}

void ATeamProjectCharacter::ClearTracerNodes()
{
	for (int i = 0; i < TracerNodes.Num(); i++) {
		if (TracerNodes[i] == NULL) 
			continue;
		TracerNodes[i]->Destroy();
	}
}

AThrowable* ATeamProjectCharacter::ThrowTracer()
{
	const FRotator spawnRot = GetControlRotation();
	FVector spawnPos = GetActorLocation() + spawnRot.RotateVector(GunOffset);
	/*
	static FName ThrowTag = FName(TEXT("ThrowableTrace"));
	FVector forwardVector = GetActorForwardVector();
	FVector EndPos = spawnPos + (forwardVector * throwRange);
	float velocitySquare;
	FCollisionQueryParams TraceParams(ThrowTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingle(Hit, spawnPos, EndPos, TraceParams,
		FCollisionObjectQueryParams::AllObjects);

	if (Hit.bBlockingHit)
	{
		float dist = FVector::Dist(Hit.ImpactPoint, spawnPos);
		velocitySquare = dist * 9.81;
	}
	*/

	AThrowable* tracer = GetWorld()->SpawnActor<AThrowable>(ProjectileClass, spawnPos, spawnRot);
	//tracer->velocitySquare = velocitySquare;
	return tracer;
}

void ATeamProjectCharacter::OnFire()
{
	//Kill();
	// try and fire a projectile
	if (bHaveThrowable)
	{
		// Drawing trajectory
		StartDrawingTracerNodes();
	}
}

void ATeamProjectCharacter::OnStopFire()
{
	if (bHaveThrowable)
	{
		StopDrawingTracerNodes();
		drawingTracer = false;
		if (ProjectileClass != NULL)
		{
			const FRotator SpawnRotation = GetControlRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = GetActorLocation() + SpawnRotation.RotateVector(GunOffset);

			UWorld* const World = GetWorld();
			if (World != NULL)
			{
				// spawn the projectile at the muzzle
				World->SpawnActor<AThrowable>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
		}

		/*
		// try and play the sound if specified
		if (FireSound != NULL)
		{
			UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}

		// try and play a firing animation if specified
		if (FireAnimation != NULL)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
			if (AnimInstance != NULL)
			{
				AnimInstance->Montage_Play(FireAnimation, 1.f);
			}
		}
		*/
	}
	bHaveThrowable = false;
}

void ATeamProjectCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// only fire for first finger down
	if (FingerIndex == 0)
	{
		OnFire();
	}
}

void ATeamProjectCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ATeamProjectCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ATeamProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATeamProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATeamProjectCharacter::PreKill()
{

}

void ATeamProjectCharacter::Kill(){
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("Dead!!!"));
	dead = true;
	PlayDeath();
	UnPossessed();
}

void ATeamProjectCharacter::Tick(float DeltaSeconds){
	if (dead && DeathTiming < 1.25f){
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("Dead!!!"));
		//FirstPersonCameraComponent->SetWorldRotation(FRotator(45.0, 0, 0), true);
		FirstPersonCameraComponent->bUseControllerViewRotation = false;
		FirstPersonCameraComponent->AddLocalRotation(FRotator(360.0*DeltaSeconds, 0, 0), false);
		DeathTiming += DeltaSeconds;
	}
	if (Sneaking){
		Stamina -= S_Reduction_Rate*DeltaSeconds;
		if (Stamina < 0){
			Stamina = 0;
		}
	}
	else{
		if (Stamina < Stamina_Max){
			Stamina += S_Regen_Rate*DeltaSeconds;
		}
	}

	// Redraw throwable trajectory
	if (drawingTracer)
	{
		float changeInRotation = (GetActorRotation().Vector() - oldRotation.Vector()).Size();
		float changeInLocation = (GetActorLocation() - oldLocation).Size();;
		if (changeInRotation > 0.05f || changeInLocation > 5)
		{
			StopDrawingTracerNodes();
			StartDrawingTracerNodes();
		}
	}

}

void ATeamProjectCharacter::PlayDeath(){
	//FirstPersonCameraComponent->AddLocalRotation(FRotator(0, 0, 0.45f), false);
	//APlayerController* PC = GetWorld()->GetFirstPlayerController();
	PC = GetWorld()->GetFirstPlayerController();
	DeathFlash1();
}

void ATeamProjectCharacter::DeathFlash1()
{
	PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0, 0.5f), 0.5f, false);
	GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::DeathFlash2, 0.5f, false);

	if (DeathTiming > 1.25f){
		dead = false;
		GetWorldTimerManager().ClearAllTimersForObject(this);
		PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0, 1.0f), 1.0f, false);
		GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::Restart, 5.0f, false);
	}

}

void ATeamProjectCharacter::DeathFlash2()
{
	PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0.5f, 0), 0.5f, false);
	GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::DeathFlash1, 0.5f, false);
	if (DeathTiming > 1.25f){
		dead = false;
		GetWorldTimerManager().ClearAllTimersForObject(this);
		PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0, 1.0f), 1.0f, false);
		GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::RestartGame, 2.0f, false);
	}
}

void ATeamProjectCharacter::RestartGame_Implementation(){
	//TODO i hate things
}

void ATeamProjectCharacter::AddPickup(APickup* p){
	for (auto Itr(Inventory.CreateIterator()); Itr; Itr++)
	{
		if (*Itr == NULL){
			*Itr = p;
			break;
		}
		
	}
	if (p->IsA<ADoorKey>()){
		bHaveKey = true;
	}
	if (p->IsA<AThrowablePickup>()){
		bHaveThrowable = true;
	}
	PC = GetWorld()->GetFirstPlayerController();
	AHUD* Hud = PC->GetHUD();
	ATeamProjectHUD* MyHUD = Cast<ATeamProjectHUD>(Hud);
	MyHUD->AddInventory(p);
	InventorySize++;
}

void ATeamProjectCharacter::ChooseInventory(){
	int i = 0;
	bool found = false;
	if (InventorySize == 0){
		return;
	}
	for (auto Itr(Inventory.CreateIterator()); Itr; Itr++)
	{
		if (*Itr != NULL){
			if (i > CurrentChoice && (*Itr)->IsA<ADrugs>()){
				CurrentChoice = i;
				found = true;
				break;
			}
		}
		i++;
	}
	if (!found){
		i = 0;
		for (auto Itr(Inventory.CreateIterator()); Itr; Itr++)
		{
			if (*Itr != NULL && (*Itr)->IsA<ADrugs>()){
				CurrentChoice = i;
				break;
			}
			i++;
		}
	}
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Choice Number: %d"), CurrentChoice));
	PC = GetWorld()->GetFirstPlayerController();
	AHUD* Hud = PC->GetHUD();
	ATeamProjectHUD* MyHUD = Cast<ATeamProjectHUD>(Hud);
	MyHUD->ChooseInventory(CurrentChoice);
}

void ATeamProjectCharacter::UseInventory(FString s){
	if (s == "Key"){
		bHaveKey = false;
	}
	else if (s == "Throwable"){
		bHaveThrowable = false;
	}
	for (auto Itr(Inventory.CreateIterator()); Itr; Itr++)
	{
		if (Itr){
			if ((*Itr) != NULL){
				if ((*Itr)->IsA<ADoorKey>() && s == "Key"){
					*Itr = NULL;
					break;
				}
				else if ((*Itr)->IsA<AThrowablePickup>() && s == "Throwable"){
					*Itr = NULL;
					break;
				}
			}
		}
	}
	PC = GetWorld()->GetFirstPlayerController();
	AHUD* Hud = PC->GetHUD();
	ATeamProjectHUD* MyHUD = Cast<ATeamProjectHUD>(Hud);
	MyHUD->UseInventory(s);
	InventorySize--;
}

void ATeamProjectCharacter::UseDrug(){
	FString s = "ini";
	if (InventorySize == 0){
		return;
	}
	if (Inventory[CurrentChoice] != NULL){
		if (Inventory[CurrentChoice]->IsA<AInvisibleDrug>()){
			s = "InvisibleDrug";
			//TurnAround();
			AInvisibleDrug* Drug = Cast<AInvisibleDrug>(Inventory[CurrentChoice]);
			Drug->OnUse();
		}
		else if (Inventory[CurrentChoice]->IsA<AGhostDrug>()){
			s = "GhostDrug";
			AGhostDrug* Drug = Cast<AGhostDrug>(Inventory[CurrentChoice]);
			Drug->OnUse();
		}
		if (s != "ini"){
			PC = GetWorld()->GetFirstPlayerController();
			AHUD* Hud = PC->GetHUD();
			ATeamProjectHUD* MyHUD = Cast<ATeamProjectHUD>(Hud);
			MyHUD->UseInventory(s);
			InventorySize--;
		}
	}
}

void ATeamProjectCharacter::SetGhost(){
	GhostState = true;
	//ZLocation = GetActorLocation().Z;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Ghost Set")));
	GetWorldTimerManager().SetTimer(this, &ATeamProjectCharacter::StopGhost, 10, false);
}

void ATeamProjectCharacter::StopGhost(){
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Ghost State End")));
	GhostState = false;
	EndAllowGhostThroughWall();
}

void ATeamProjectCharacter::TurnAround()
{
	FRotator PlayerRot = FRotationMatrix::MakeFromX(GuardScareLocation->GetActorLocation()).Rotator();
	FRotator NewRot = FMath::RInterpTo(Mesh->GetComponentRotation(), PlayerRot, GetWorld()->DeltaTimeSeconds , 2);
}


void ATeamProjectCharacter::AllowGhostThroughWall_Implementation(){
	
}

void ATeamProjectCharacter::EndAllowGhostThroughWall_Implementation(){

}
