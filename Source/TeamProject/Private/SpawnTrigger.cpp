#include "TeamProject.h"
#include "TeamProjectCharacter.h"
#include "SpawnTrigger.h"


ASpawnTrigger::ASpawnTrigger(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	SpawnMesh = PCIP.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("SpawnMesh"));
	RootComponent = SpawnMesh;

	Box = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Box"));
	Box->bGenerateOverlapEvents = true;
	Box->SetRelativeScale3D(FVector(2, 2, 3));
	RootComponent = Box;

	spawnedOnce = false;
	//FVector 
	//SpawnLocation = FVector(0, 0, 0);
	//SpawnRotation = FRotator(0, 0, 0);

	//Box->OnComponentEndOverlap.AddDynamic(this, &ASpawnTrigger::TriggerExit);
	Box->OnComponentBeginOverlap.AddDynamic(this, &ASpawnTrigger::TriggerEnter);
}

void ASpawnTrigger::TriggerEnter(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (!spawnedOnce)
	{
		if (OtherActor){
			if (OtherActor->IsA<ATeamProjectCharacter>()){
				SpawnedGuard = SpawnGuard();
				PlayMySound(MySound);
				GetWorldTimerManager().SetTimer(this, &ASpawnTrigger::FadeOut, 2.0f, false);
			}
		}
	}
	//Debug("Trigger Enter");
}

void ASpawnTrigger::FadeOut()
{
	SpawnedGuard->PlayAttack();
	PC = GetWorld()->GetFirstPlayerController();
	PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0, 1.0f), 2.0f, false);
	GetWorldTimerManager().SetTimer(this, &ASpawnTrigger::FadeIn, 2.0f, false);
}
//void ASpawnTrigger::TriggerExit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
//{
//
//		if (OtherActor){
//			if (OtherActor->IsA<ATeamProjectCharacter>()){
//				//this->Destroy();
//				PC = GetWorld()->GetFirstPlayerController();
//				PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0, 1.0f), 2.0f, false);
//				//PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0, 0.5f), 0.5f, false);
//				GetWorldTimerManager().SetTimer(this, &ASpawnTrigger::FadeIn, 2.0f, false);
//				//OtherActor->
//				/*FVector SpawnLocation = PlayerSpawnPoint->GetActorLocation();
//				OtherActor->SetActorLocation(SpawnLocation);*/
//			}
//		}
//
//	
//	//Debug("Trigger Exit");
//}

void ASpawnTrigger::FadeIn()
{
	APawn* playerPawn = PC->GetPawn();
	if (playerPawn)
	{
		FVector SpawnLocation = PlayerSpawnPoint->GetActorLocation();
		playerPawn->SetActorLocation(SpawnLocation);
		//playerPawn->GetActor->SetActorLocation(SpawnLocation);
	}
	PC->ClientSetCameraFade(true, FColor(0, 0, 0), FVector2D(0, 0), 3.0f, false);
	//OtherActor->SetActorLocation(SpawnLocation);
}

void ASpawnTrigger::Debug(FString Msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, Msg);
	}
}

AGuardCharacter* ASpawnTrigger::SpawnGuard()
{
	//AGuardCharacter* myGuard = NULL;
	FActorSpawnParameters SpawnParams;
	if (SpawnClass)
	{
		//Debug("got spawn class");
		UWorld* World = GetWorld();
		if (World)
		{

			FActorSpawnParameters SpawnParams;
			SpawnParams.Instigator = Instigator;
			FRotator SpawnRotation;
			SpawnRotation.Yaw = 270.f;//90.f;
			SpawnRotation.Pitch = 0;
			SpawnRotation.Roll = 0;

			FVector SpawnLocation = SpawnPoint->GetActorLocation();

			AGuardCharacter* /*const*/ myGuard = World->SpawnActor<AGuardCharacter>(SpawnClass, SpawnLocation, SpawnRotation, SpawnParams);
			spawnedOnce = true;
			return myGuard;
		}
	}
	return NULL;
	//myGuard = UWorld::SpawnActor<AGuardCharacter>(AGuardCharacter, &SpawnLocation, &SpawnRotation, NULL);

	//myGuard = (AGuardCharacter*)UWorld::SpawnActor<AGuardCharacter>(AGuardCharacter::StaticClass());
	//return myGuard;
}

UAudioComponent* ASpawnTrigger::PlayMySound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::PlaySoundAttached(Sound, RootComponent);
	}
	return AC;
}