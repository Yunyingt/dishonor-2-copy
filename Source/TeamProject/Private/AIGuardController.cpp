

#include "TeamProject.h"
#include "AIGuardController.h"
#include "GuardCharacter.h"
//testing
#include "Engine.h"
#include "TeamProjectCharacter.h"



AAIGuardController::AAIGuardController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	CurrentState = Start;
	
	
	patrolIdx = 0;
	SearchTime = 7.0f;
}

void AAIGuardController::BeginPlay()
{
	Super::BeginPlay();
	UWorld* World =  GetWorld();
	if (World)
	{
		MyPawn = Cast<AGuardCharacter>(GetPawn());
		if (MyPawn)
		{
			AGuardCharacter* MyCharacter = Cast<AGuardCharacter>(MyPawn);
			if (MyCharacter)
			{
				TargetPoints = MyCharacter->GetPatrolPoints();
			}
			//get player pawn
			PlayerPawn = UGameplayStatics::GetPlayerPawn(World, 0);
			if (PlayerPawn)
			{
				//AGuardCharacter* MyPawn = Cast<AGuardCharacter>(GetPawn()); 
				//MyPawn->SetActorLocation(PlayerPawn->GetActorLocation());
				//MoveToActor(PlayerPawn);
			}
		}
	
	}

}

void AAIGuardController::Tick(float deltaTime)
{
	
	AGuardCharacter* MyCharacter = Cast<AGuardCharacter>(MyPawn);
	ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
	

	//just spawned, start patrol
	if (CurrentState == Start)
	{
		CurrentState = Patrol;
	}

	//if my character heard bottle
	//if i am either in patrol or investigate state
		//then investigate
	//else
		//tell character bottle is false



	//saw while investigating, move to them and run!
	if (CurrentState == Chase)
	{
		MoveToLocation(lastPlayerLocation);
		MyCharacter->PlayChase();
	}

	//called if im chasing and player is close, always hits, and kills character. game ends
	if (CurrentState == Attack)
	{
		CurrentState = End;
		ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
		if (PlayerCharacter)
		{
			MyCharacter->PlayAttack();
			KillPlayer();
			//PlayerCharacter->Kill();
			/*if (!GetWorldTimerManager().IsTimerActive(this, &AAIGuardController::KillPlayer))
			{
				GetWorldTimerManager().SetTimer(this, &AAIGuardController::KillPlayer, MyCharacter->PlayAttack(), false);
			}*/
			
		}
	}

	//investigate, move towards seen location of player, pause, look around, return to patrol
	if (CurrentState == Investigate)
	{
		
		if (PlayerPawn)
		{
			if (MyCharacter->SightlineDetection() && !PlayerCharacter->isInvisible())//if i see character, pursue
			{
				StateChange();
			}
			//set timer, if dont see player, go back to patrol
			MoveToLocation(lastPlayerLocation);
			//MoveToActor(PlayerPawn);
		}
	}

	//default state, walks to each set patrol point, stops for some time, then goes to next one in a cycle
	if (CurrentState == Patrol)
	{
		MyPawn = Cast<AGuardCharacter>(GetPawn());
		if (MyPawn)
		{
			MyCharacter->StopChase();
			MyCharacter->ToggleMoveSpeedWalk();

			MoveToActor(TargetPoints[patrolIdx]);
			if (PlayerPawn)
			{
				//if the player is in my line of sight
				//if (MyPawn->GetDistanceTo(PlayerPawn) < SightRadius)
				//{
				//	CurrentState = Investigate;
				//}
			}
		}
		

	}

	if (CurrentState != Attack && CurrentState != Chase && CurrentState != End && MyCharacter->SightlineDetection() && !PlayerCharacter->isInvisible())
	{
		CurrentState = Chase;
		//ChaseAC = PlayChaseSound(ChaseLoopSound);
	}
	/*else
	{
		ChaseAC->Stop();
		PlayChaseSound(ChaseFinishSound);
	}*/
}

void AAIGuardController::ChangeStatePatrol()
{
	CurrentState = Patrol;
}

void AAIGuardController::StateChange()
{
	//from investigating, Guard will either SEE player and CHASE, or timer run out, and  back to PATROL
	GetWorldTimerManager().ClearAllTimersForObject(this);
	AGuardCharacter* MyCharacter = Cast<AGuardCharacter>(MyPawn);
	if (CurrentState == Investigate)
	{
		MyCharacter->ToggleMoveSpeedRun();
		CurrentState = Chase;
	}
	else if (CurrentState == Chase)
	{
		CurrentState = Attack;
	}
}

void AAIGuardController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	AGuardCharacter* MyCharacter = Cast<AGuardCharacter>(MyPawn);
	if (CurrentState == Investigate)
	{
		//if im close to player, chase player
		if (PlayerPawn)
		{
			float distToPlayer = MyPawn->GetDistanceTo(PlayerPawn);
			ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
			if (MyCharacter->SightlineDetection() && !PlayerCharacter->isInvisible())
			{
				StateChange();
				//we got you player!
			}
			else if (distToPlayer < MyCharacter->SoundRadius && !PlayerCharacter->IsSneaking())
			{
				lastPlayerLocation = PlayerPawn->GetActorLocation();
			}
		}
	}
	if (CurrentState == Patrol)
	{
		
		float distance = MyPawn->GetDistanceTo(TargetPoints[patrolIdx]);
		if (distance <  150.0f)
		{
			if (!GetWorldTimerManager().IsTimerActive(this, &AAIGuardController::NextPatrolPoint))
			{
				GetWorldTimerManager().SetTimer(this, &AAIGuardController::NextPatrolPoint, SearchTime, false);
			}
			
		}
		//if im close to player, chase player
		if (PlayerPawn)
		{
			float distToPlayer = MyPawn->GetDistanceTo(PlayerPawn);
			ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
			//if I hear player, investigate
			if (distToPlayer <  MyCharacter->SoundRadius && !PlayerCharacter->IsSneaking())
			{
				lastPlayerLocation = PlayerPawn->GetActorLocation();
				GetWorldTimerManager().SetTimer(this, &AAIGuardController::ChangeStatePatrol, SearchTime, false);
				CurrentState = Investigate;
				
				//we got you player!
			}
		}
	}

	//pursue while in sightline, if lose sight, go to investigate
	if (CurrentState == Chase)
	{

		//if im close to player, chase player
		if (PlayerPawn)
		{
			float distToPlayer = MyPawn->GetDistanceTo(PlayerPawn);
			if (distToPlayer <  MyCharacter->SoundRadius)
			{
				lastPlayerLocation = PlayerPawn->GetActorLocation();
				//we got you player!
			}
			if (distToPlayer < MyCharacter->AttackRange)
			{
				StateChange();
			}
			else
			{
				GetWorldTimerManager().SetTimer(this, &AAIGuardController::ChangeStatePatrol, SearchTime, true);
				CurrentState = Investigate;
				//MyCharacter->StopChase();
				//MyCharacter->ToggleMoveSpeedWalk();
				
			}

		}
	}

	//patrolIdx++;
}

void AAIGuardController::NextPatrolPoint()
{
	patrolIdx++;
	if (patrolIdx >= TargetPoints.Num())
	{
		patrolIdx = patrolIdx - TargetPoints.Num();
	}
}

void AAIGuardController::KillPlayer()
{
	ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
	if (PlayerCharacter)
	{
		PlayerCharacter->Kill();
	}
}



