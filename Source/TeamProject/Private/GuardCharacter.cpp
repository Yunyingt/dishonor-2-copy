

#include "TeamProject.h"
#include "GuardCharacter.h"
#include "TeamProjectCharacter.h"
#include "AIGuardController.h"


AGuardCharacter::AGuardCharacter(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	AIControllerClass = AAIGuardController::StaticClass();


	//DetectVolume = PCIP.CreateDefaultSubobject<UConeComponent>(this, TEXT("DetectVolume"));
	//DetectVolume->AttachTo(RootComponent);

	DetectCharacter = false;

	//true when actually punching in attack
	isAttacking = false;
	//CharacterMovement->MaxWalkSpeed = 300;

	//for playing chase music just upon entering chase
	isChasing = false;
}

void AGuardCharacter::BeginPlay()
{

	Super::BeginPlay();
	UWorld* World = GetWorld();
	if (World)
	{
		CharacterMovement->MaxWalkSpeed = WalkSpeed;
		//get player pawn
		APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(World, 0);
		if (PlayerPawn)
		{
			//
			//SetActorLocation(PlayerPawn->GetActorLocation());
			//MoveToActor(PlayerPawn);
		}
	}

}



void AGuardCharacter::OnDetectVolumnBeginOverlap(AActor* c){
	if (c){
		if (c->IsA<ATeamProjectCharacter>()){
			if (ConeSightlineDetection()){

				DetectCharacter = true;
			}
			else{
				DetectCharacter = false;
			}
		}
	}
}

void AGuardCharacter::OnDetectVolumnEndOverlap(AActor* c){
	if (c){
		if (c->IsA<ATeamProjectCharacter>()){
			DetectCharacter = false;
		}
	}
}

bool AGuardCharacter::SightlineDetection(){
	static FName WeaponFireTag = FName(TEXT("Guard Sightline"));
	FVector StartPos = GetActorLocation();
	StartPos.Z = StartPos.Z - 50.0f;
	FVector EndPos = StartPos + 300.0f*GetActorForwardVector();
	
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	//This fires the ray and checks against all objects w/ collision
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingle(Hit, StartPos, EndPos, TraceParams, FCollisionObjectQueryParams::AllObjects);

	if (Hit.bBlockingHit){
		//PlayHitEffect(HitEF, Hit.Location);
		AActor* a = Hit.GetActor();
		if (a)
		{
			if (a->IsA<ATeamProjectCharacter>()){
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Sightline: Hit Player Character"));
				return true;
			}
			else{
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Sightline: Blocked by object. Not seeing player character"));
				return false;
			}
		}
		else
		{
			return false;
		}

	}
	else{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("Sightline: Doesn't hit anything but the player is within the range"));
		return DetectCharacter;
	}


}

bool AGuardCharacter::ConeSightlineDetection(){
	static FName WeaponFireTag = FName(TEXT("Guard Sightline"));
	FVector StartPos = GetActorLocation();
	StartPos.Z = StartPos.Z - 50.0f;
	FVector EndPos = StartPos + 300.0f*GetActorForwardVector();
	FVector EndPosUp = EndPos + FVector(0.0f, 0.0f, 100.0f);
	FVector EndPosDown = EndPos + FVector(0.0f, 0.0f, -100.0f);
	FVector EndPosLeft = EndPos + FVector(0.0f, -100.0f, 0.0f);
	FVector EndPosRight = EndPos + FVector(0.0f, 100.0f, 0.0f);

	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	//This fires the ray and checks against all objects w/ collision
	FHitResult Hit(ForceInit);
	FHitResult Hit2(ForceInit);
	FHitResult Hit3(ForceInit);
	FHitResult Hit4(ForceInit);
	FHitResult Hit5(ForceInit);
	GetWorld()->LineTraceSingle(Hit, StartPos, EndPos, TraceParams, FCollisionObjectQueryParams::AllObjects);
	GetWorld()->LineTraceSingle(Hit2, StartPos, EndPosUp, TraceParams, FCollisionObjectQueryParams::AllObjects);
	GetWorld()->LineTraceSingle(Hit3, StartPos, EndPosDown, TraceParams, FCollisionObjectQueryParams::AllObjects);
	GetWorld()->LineTraceSingle(Hit4, StartPos, EndPosLeft, TraceParams, FCollisionObjectQueryParams::AllObjects);
	GetWorld()->LineTraceSingle(Hit5, StartPos, EndPosRight, TraceParams, FCollisionObjectQueryParams::AllObjects);
	AActor* a = NULL;
	if (Hit.bBlockingHit){
		//PlayHitEffect(HitEF, Hit.Location);
		a = Hit.GetActor();
	}
	else if (Hit2.bBlockingHit){
		a = Hit2.GetActor();
	}
	else if (Hit3.bBlockingHit){
		a = Hit3.GetActor();
	}
	else if (Hit4.bBlockingHit){
		a = Hit4.GetActor();
	}
	else if (Hit5.bBlockingHit){
		a = Hit5.GetActor();
	}
	if (a)
	{
		if (a->IsA<ATeamProjectCharacter>()){
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Sightline: Hit Player Character"));
			ATeamProjectCharacter* c = Cast<ATeamProjectCharacter>(a);
			if (c){
				if (!c->isInvisible()){
					return true;
				}
			}
			return false;
			
		}
		else{
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Sightline: Blocked by object. Not seeing player character"));
			return false;
		}
	}

	else{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("Sightline: Doesn't hit anything but the player is within the range"));
		return DetectCharacter;
	}


}


void AGuardCharacter::PlayChase(/*const FVector& lastPlayerLocation*/)
{
	//MoveToLocation(lastPlayerLocation);
	if (!isChasing)
	{
		ChaseAC = PlayChaseSound(ChaseLoopSound);
		isChasing = true;
	}	
}

void AGuardCharacter::StopChase(/*const FVector& lastPlayerLocation*/)
{
	if (isChasing)
	{
		StopChaseSound(ChaseFinishSound);
	}
}

UAudioComponent* AGuardCharacter::PlayChaseSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::PlaySoundAttached(Sound, RootComponent);
	}
	return AC;
}

void AGuardCharacter::StopChaseSound(USoundCue* Sound)
{
	if (isChasing)
	{
		ChaseAC->Stop();
	}
	if (Sound)
	{
		PlayChaseSound(Sound/*ChaseFinishSound*/);
		isChasing = false;
	}


}

/*
void AGuardCharacter::KillPlayer()
{
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
	if (PlayerCharacter)
	{
		PlayerCharacter->Kill();
	}
}
*/


UParticleSystemComponent* AGuardCharacter::PlayHitEffect(UParticleSystem* Particle, FVector end){
	UParticleSystemComponent* EF = NULL;
	if (Particle){
		EF = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, end);
		//print("Play Fire Effect");
	}

	return EF;
}


void AGuardCharacter::HearBottle(FVector Location)
{
	AAIGuardController* MyController = Cast<AAIGuardController>(GetController());
	MyController->HearBottle(Location);
}

