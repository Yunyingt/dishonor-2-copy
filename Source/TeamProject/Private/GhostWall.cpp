

#include "TeamProject.h"
#include "TeamProjectCharacter.h"
#include "GhostWall.h"


AGhostWall::AGhostWall(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PrimaryActorTick.bCanEverTick = true;
	WallMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("WallMesh"));
	SetRootComponent(WallMesh);

	TriggerVolume = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("TriggerVolume"));
	TriggerVolume->AttachTo(RootComponent);
	//TriggerVolume->AttachTo(WallMesh);
}

void AGhostWall::OnComponentBeginOverlap(AActor* a){
	if (a){
		if (a->IsA<ATeamProjectCharacter>()){
			ATeamProjectCharacter* Character = Cast<ATeamProjectCharacter>(a);
			if (Character->GhostState){
				WallMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				//Character->AllowGhostThroughWall();
			}
		}
	}
}

void AGhostWall::OnComponentEndOverlap(AActor* a){
	FString name = a->GetActorClass()->GetName();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, name);
	if (a){
		if (a->IsA<ATeamProjectCharacter>()){
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("C++ End Overlap ")));
			ATeamProjectCharacter* Character = Cast<ATeamProjectCharacter>(a);
			//WallMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
			GetWorldTimerManager().SetTimer(this, &AGhostWall::StopCollision, 1, false);
			
			//Character->EndAllowGhostThroughWall();
		}
		else{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("C++ End Overlap Called Not Character")));
		}
	}
}

void AGhostWall::StopCollision(){
	WallMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

void AGhostWall::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (playerActor){
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (player){
			if (player->GhostState){

			}
		}
	}
	

}

