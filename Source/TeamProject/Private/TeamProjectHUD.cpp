// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "TeamProject.h"
#include "TeamProjectHUD.h"
#include "TeamProjectCharacter.h"
#include "Engine/Canvas.h"
#include "DoorKey.h"
#include "InvisibleDrug.h"
#include "GhostDrug.h"
#include "ThrowablePickup.h"
#include "TextureResource.h"
#include "CanvasItem.h"

ATeamProjectHUD::ATeamProjectHUD(const class FPostConstructInitializeProperties& PCIP) : Super(PCIP)
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshiarTexObj(TEXT("/Game/Textures/Crosshair"));
	CrosshairTex = CrosshiarTexObj.Object;

	static ConstructorHelpers::FObjectFinder<UFont>HUDFontOb(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	HUDFont = HUDFontOb.Object;

	static ConstructorHelpers::FObjectFinder<UMaterialInstance>StaminaMI(TEXT("/Game/Materials/ExampleContent/HUD/Materials/M_HUD_Instance"));
	UMaterialInstance* m = StaminaMI.Object;
	if (m == NULL){
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Material is null"));
	}
	else{
		StaminaBarMI = UMaterialInstanceDynamic::Create(m, this);
		PowerTimerMI = UMaterialInstanceDynamic::Create(m, this);
	}

	static ConstructorHelpers::FObjectFinder<UMaterial>InvObj(TEXT("/Game/Blueprints/Inventory/Inventory_Mat"));
	InventoryBgrd = InvObj.Object;

	static ConstructorHelpers::FObjectFinder<UMaterial>KeyObj(TEXT("/Game/Blueprints/Inventory/Key_Mat"));
	KeyInv = KeyObj.Object;

	static ConstructorHelpers::FObjectFinder<UMaterial>Drug1(TEXT("/Game/Blueprints/Inventory/Invisible_Mat"));
	InvisibleInv = Drug1.Object;

	static ConstructorHelpers::FObjectFinder<UMaterial>Drug2(TEXT("/Game/Blueprints/Inventory/Ghost_Mat"));
	GhostInv = Drug2.Object;

	static ConstructorHelpers::FObjectFinder<UMaterial>Bottle(TEXT("/Game/Blueprints/Inventory/Bottle_Mat"));
	BottleInv = Bottle.Object;

	Inventories.Init(NULL, 6);
	Offset = 73.0f;
	LeftOffset = 25.0f;
	UpOffset = 28.0f;
	CurrentChoice = -1;
	Timer = 10.0f;
}


void ATeamProjectHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X - (CrosshairTex->GetSurfaceWidth() * 0.5)),
										   (Center.Y - (CrosshairTex->GetSurfaceHeight() * 0.5f)) );

	InventoryDisplayPos = FVector2D(Canvas->ClipX * 0.76f, Canvas->ClipY * 0.71f);

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );

	ShowStamina();
	ShowInventory();
	ShowInventoryChoice();
	ShowPowerTimer();
}

void ATeamProjectHUD::ShowStamina(){
	
	ATeamProjectCharacter* c = Cast<ATeamProjectCharacter>(GetOwningPawn());
	if (c){
		FString HPString = FString::Printf(TEXT("Stamina %3.0f"), c->Stamina);
		DrawText(HPString, FColor::White, 50, 50, HUDFont, 1.0f);
		float p = (c->Stamina) / (c->Stamina_Max);
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, FString::Printf(TEXT("Percentage: %f"), p));
		StaminaBarMI->SetScalarParameterValue(FName("Health Percentage"), p);
		DrawMaterialSimple(StaminaBarMI, 200, 50, 200, 25, 1, false);//OCCASIONAL EXCEPTION THROWN HERE
	}
}

void ATeamProjectHUD::ShowInventory(){
	//DrawTexture(InventoryBgrd, InventoryDisplayPos.X, InventoryDisplayPos.Y, InventoryBgrd->GetSizeX(), InventoryBgrd->GetSizeY(), 0, 0, 1, 1, FLinearColor::White, BLEND_Opaque);
	DrawMaterialSimple(InventoryBgrd, InventoryDisplayPos.X, InventoryDisplayPos.Y, 256.0, 256.0, 1, false);
	int32 i = 0;
	for (auto Itr(Inventories.CreateIterator()); Itr; Itr++)
	{
		if ((*Itr) != NULL){
			if ((*Itr)->IsA<ADoorKey>()){
				DrawMaterialSimple(KeyInv, InventoryDisplayPos.X + LeftOffset + i*Offset, InventoryDisplayPos.Y + UpOffset + i / 3 * Offset, 64.0, 64.0);
			}
			else if ((*Itr)->IsA<AInvisibleDrug>()){
				DrawMaterialSimple(InvisibleInv, InventoryDisplayPos.X + LeftOffset + i*Offset, InventoryDisplayPos.Y + UpOffset + i / 3 * Offset, 64.0, 64.0);
			}
			else if ((*Itr)->IsA<AGhostDrug>()){
				DrawMaterialSimple(GhostInv, InventoryDisplayPos.X + LeftOffset + i*Offset, InventoryDisplayPos.Y + UpOffset + i / 3 * Offset, 64.0, 64.0);
			}
			else if ((*Itr)->IsA<AThrowablePickup>()){
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Here")));
				DrawMaterialSimple(BottleInv, InventoryDisplayPos.X + LeftOffset + i*Offset, InventoryDisplayPos.Y + UpOffset + i / 3 * Offset, 64.0, 64.0);
			}
			i++;
		}
	}
}

void ATeamProjectHUD::ShowInventoryChoice(){
	FString name = "Current Choice: ";
	FString inv;
	if (CurrentChoice >= 0 && Inventories[CurrentChoice] != NULL){
		if (Inventories[CurrentChoice]->IsA<ADoorKey>()){
			inv = "Key";
		}
		else if (Inventories[CurrentChoice]->IsA<AInvisibleDrug>()){
			inv = "InvisibleDrug";
		}
		else if (Inventories[CurrentChoice]->IsA<AGhostDrug>()){
			inv = "GhostDrug";
		}
		else if (Inventories[CurrentChoice]->IsA<AThrowablePickup>()){
			inv = "Throwable";
		}
	}
	else{
		inv = "";
	}
	name = name + inv;
	DrawText(name, FColor::White, 50, 100, HUDFont);
}

void ATeamProjectHUD::AddInventory(APickup* p){
	float i = 0;
	for (auto Itr(Inventories.CreateIterator()); Itr; Itr++)
	{
		if (*Itr == NULL){
			*Itr = p;
			break;
		}
		i++;
	}
}

void ATeamProjectHUD::UseInventory(FString s){
	for (auto Itr(Inventories.CreateIterator()); Itr; Itr++)
	{
		if (Itr){
			if ((*Itr) != NULL){
				if ((*Itr)->IsA<ADoorKey>() && s == "Key"){
					*Itr = NULL;
					break;
				}
				else if ((*Itr)->IsA<AInvisibleDrug>() && s == "InvisibleDrug"){
					*Itr = NULL;
					break;
				}
				else if ((*Itr)->IsA<AGhostDrug>() && s == "GhostDrug"){
					*Itr = NULL;
					break;
				}
				else if ((*Itr)->IsA<AThrowablePickup>() && s == "Throwable"){
					*Itr = NULL;
					break;
				}
			}
		}
	}
	/*AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (playerActor){
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (player){
			Inventories.Empty();
			Inventories = player->Inventory;
		}
	}*/
}

void ATeamProjectHUD::ChooseInventory(int i){
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (playerActor){
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (player){
			Inventories.Empty();
			Inventories = player->Inventory;
		}
	}
	CurrentChoice = i;

}

void ATeamProjectHUD::ShowPowerTimer(){
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (playerActor){
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (player){
			if (player->GhostState || player->isInvisible()){
				FString HPString = FString::Printf(TEXT("Power Time"));
				DrawText(HPString, FColor::White, 450, 50, HUDFont, 1.0f);
				float t = Timer / 10;
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, FString::Printf(TEXT("Percentage: %f"), p));
				if (PowerTimerMI){
					PowerTimerMI->SetScalarParameterValue(FName("Health Percentage"), t);
					DrawMaterialSimple(PowerTimerMI, 600, 50, 200, 25, 1, false);//OCCASIONAL EXCEPTION THROWN HERE
				}
			}
		}
	}
}

void ATeamProjectHUD::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
	AActor* playerActor = Cast<AActor>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (playerActor){
		ATeamProjectCharacter* player = Cast<ATeamProjectCharacter>(playerActor);
		if (player){
			if (player->GhostState || player->isInvisible()){
				Timer -= DeltaSeconds;
			}
			else{
				Timer = 10.0f;
			}
		}
	}
}
