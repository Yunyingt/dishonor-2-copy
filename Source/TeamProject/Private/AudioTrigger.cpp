

#include "TeamProject.h"
#include "TeamProjectCharacter.h"
#include "AudioTrigger.h"


AAudioTrigger::AAudioTrigger(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	Box = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Box"));
	Box->bGenerateOverlapEvents = true;
	Box->SetRelativeScale3D(FVector(2, 2, 3));
	RootComponent = Box;

	Box->OnComponentEndOverlap.AddDynamic(this, &AAudioTrigger::TriggerExit);
	Box->OnComponentBeginOverlap.AddDynamic(this, &AAudioTrigger::TriggerEnter);
}

void AAudioTrigger::TriggerEnter(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor){
		if (OtherActor->IsA<ATeamProjectCharacter>()){
			PlayMySound(MySound);
		}
	}
	//Debug("Trigger Enter");
}

void AAudioTrigger::TriggerExit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor){
		if (OtherActor->IsA<ATeamProjectCharacter>()){
			this->Destroy();
		}
	}
	//Debug("Trigger Exit");
}

void AAudioTrigger::Debug(FString Msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, Msg);
	}
}

UAudioComponent* AAudioTrigger::PlayMySound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::PlaySoundAttached(Sound, RootComponent);
	}
	return AC;
}
