

#include "TeamProject.h"
#include "EnemyCharacter.h"
#include "AIEnemyController.h"


AEnemyCharacter::AEnemyCharacter(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}


void AEnemyCharacter::ToggleMoveSpeedWalk()
{
	CharacterMovement->MaxWalkSpeed = WalkSpeed;
}

void AEnemyCharacter::ToggleMoveSpeedRun()
{
	CharacterMovement->MaxWalkSpeed = RunSpeed;
}

TArray<AActor*> AEnemyCharacter::GetPatrolPoints()
{
	return TargetPoints;
}

void AEnemyCharacter::HearBottle(FVector Location)
{
	AAIEnemyController* MyController = Cast<AAIEnemyController>(GetController());
	if (MyController)
	{
		MyController->HearBottle(Location);
	}

}

float AEnemyCharacter::PlayAttack()
{
	isAttacking = true;

	return PlayAnimMontage(AttackAnim);
	if (AttackAnim != NULL)
	{
		//GetWorldTimerManager().SetTimer(this, &AGuardCharacter::KillPlayer, PlayAnimMontage(AttackAnim), true);
	}
}
